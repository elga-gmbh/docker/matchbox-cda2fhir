# Matchbox CDA2FHIR

Make use of [Matchbox](https://github.com/ahdis/matchbox) together with the [CDA Logical Model](https://github.com/HL7Austria/CDA-core-2.0) which also contains Austrian enhancements.

> Note: Currently a fixed version of the CDA Logical Model is being used.

## Create a new docker container

    docker-compose up